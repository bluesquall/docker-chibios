FROM debian:bullseye-slim
MAINTAINER https://bitbucket.org/bluesquall/docker-ChibiOS/issues

RUN apt-get update \
 && apt-get install --yes --no-install-recommends \
      build-essential \
      curl \
      ca-certificates \
      gcc-arm-none-eabi \
      libnewlib-arm-none-eabi \
      libstdc++-arm-none-eabi-newlib \
      sudo \
 && apt-get autoremove --yes && apt-get clean

ARG USERNAME=artoo
ARG USER_UID=18242
ARG USER_GID=${USER_UID}

RUN groupadd --gid ${USER_GID} ${USERNAME} \
 && useradd --uid ${USER_UID} --gid ${USER_GID} --shell /bin/bash --create-home ${USERNAME} \
 && echo ${USERNAME} ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/${USERNAME} \
 && chmod 0440 /etc/sudoers.d/${USERNAME}

USER ${USERNAME}
ENV HOME /home/${USERNAME}
WORKDIR ${HOME}

